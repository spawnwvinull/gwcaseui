const server = "localhost";

const config = {
  subscriptionUrl: "https://" + server + ":5001/subscription",
  priceCalculationUrl: "http://" + server + ":5000/subscription"
};

export default config;
