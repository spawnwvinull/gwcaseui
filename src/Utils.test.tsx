import { resolveCurrency, resolveDropDownListElements } from "./Utils";
import { Subscriptions } from "./interfaces/Data";


test('resolve euro symbol', () => {
  // arrange
  let currencyType = "0"

  // act
  let result = resolveCurrency(currencyType);

  // assert 
  expect(result).toBe('\u20AC');

});

test('create dropdownelemnts', () => {
  // arrange setup fake data
  const data: Subscriptions = {
    subscriptionItems: [{
      id: "",
      name: "Occasional",
      vehicleTypes: [{
          id: "",
          name: "Small",
          pricePerHour: { 
            amount: 10,
            currency: "Euro",
            id: ""
          },
          pricePerKilometer: {
            id: "",
            amount: 20,
            currency: "Euro"
          }
      }
    ]}]
  }

  // act
  let result = resolveDropDownListElements(data);

  // assert
  expect(result).toBeDefined();
  expect(result.SubscriptionNames).toBeDefined();
  expect(result.SubscriptionNames.length).toEqual(1);
  expect(result.SubscriptionNames[0]).toEqual("Occasional");
  expect(result.VehicleTypeNames).toBeDefined();
  expect(result.VehicleTypeNames.length).toEqual(1)
  expect(result.VehicleTypeNames[0]).toEqual("Small")
})


