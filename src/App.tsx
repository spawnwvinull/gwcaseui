import React, { useState, useEffect } from "react";
import './App.css';
import Grid from "@material-ui/core/Grid";
import { Subscription, Subscriptions, DropdownElements } from "./interfaces/Data";
import { getSubscriptionsPromise } from "./DataComponent";
import config from "react-global-configuration";
import configuration from "./config";
import SubscriptionSet from "./SubscriptionElement";
import PriceCalculator from "./PriceCalculator";
import { resolveDropDownListElements } from "./Utils";

const initialDropDownElements: DropdownElements = {
  SubscriptionNames: [],
  VehicleTypeNames: []
}
config.set(configuration)
function App(): JSX.Element{
  const [subscriptions, setSubscriptions] = useState<Subscriptions>();
  const [dropDownElements, setupDropDownElements] = useState<DropdownElements>(initialDropDownElements);
  
  useEffect(() => {
    const dataUrl = config.get("subscriptionUrl");
    const fetchData = (): void => {
      console.log("start data transfer")
      getSubscriptionsPromise(dataUrl)
        .then((data: Subscriptions) => {
          setSubscriptions(data)
          console.log(data)
          setupDropDownElements(resolveDropDownListElements(data))
        })
        .catch(error => {
          console.log(error);
        });
    };

    fetchData();
  }, []);

  return (
    <Grid>
      <PriceCalculator { ...dropDownElements }/>
      <h1>GreenWheels vehicle subcription overview</h1>
      {subscriptions?.subscriptionItems.map((item: Subscription) => (
          <SubscriptionSet { ...item } />  
        ))}
    </Grid>
  );
}

export default App;
