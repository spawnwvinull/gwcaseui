import { Subscriptions, Subscription, VehicleType, Price, ActualSubscription } from "../interfaces/Data";

const testPrice: Price = {
  id: "",
  amount: 10,
  currency: "Euro"
}

const testVehicle: VehicleType = {
  id: "",
  name: "testVehicle",
  pricePerHour: testPrice,
  pricePerKilometer: testPrice
}

const testSubscription: Subscription = {
  id: "", 
  name: "testSubscription",
  vehicleTypes: [testVehicle]
};

const testSubScriptions: Subscriptions = {
  subscriptionItems: [testSubscription, testSubscription, testSubscription]
}

export function getSubscriptionsPromise(subscriptionUrl: string): Promise<Subscriptions> {
  return new Promise<Subscriptions>((resolve, reject) => {
    if (subscriptionUrl) {
      resolve(testSubScriptions);
    } else {
      reject(undefined);
    }
  });
}

export function sendDataForCalculation(subscriptionUrl: string, data: ActualSubscription): Promise<number> {
  return new Promise<number>((resolve, reject) => {
    if (subscriptionUrl) {
      resolve(608.16);
    } else {
      reject(undefined);
    }
  })
}
