import { Subscriptions, DropdownElements } from "./interfaces/Data";

export function resolveCurrency(currency:string): string {
    console.log("currency");
    console.log(currency);
    if (currency.toString() === "0") {
        return '\u20AC'}
    else {
        return "";
    }
}

export function resolveAmount(amount: number): string {
    return amount.toFixed(2);
}

export function resolveDropDownListElements(subscriptions: Subscriptions): DropdownElements {
    let dropdownElements: DropdownElements = {
        SubscriptionNames: subscriptions.subscriptionItems.map(x => x.name),
        VehicleTypeNames: subscriptions.subscriptionItems[0].vehicleTypes.map(x => x.name)
    }
    return dropdownElements;
}
