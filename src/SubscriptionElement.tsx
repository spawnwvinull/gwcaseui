import Grid from "@material-ui/core/Grid";
import React from "react";
import { Subscription, VehicleType } from "./interfaces/Data";
import VehicleTypeElement from "./VehicleTypeElement";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";

function SubScriptionElement(
    item
: Subscription): JSX.Element {
    return (
    <Grid id={item.id}>
        <Card>
            <CardHeader title={ item.name }>
        </CardHeader>
        <CardContent>
        { item.vehicleTypes.map((item: VehicleType) => (
            <VehicleTypeElement { ...item } />
        ))}
        </CardContent>
        </Card>
    </Grid>
    )
}

export default SubScriptionElement 