import { Grid } from "@material-ui/core";
import { VehicleType } from "./interfaces/Data";
import React from "react";
import HourPriceElement from "./HourPriceElement"
import KilometerPriceElement from "./KilometerPriceElement"

function VehicleTypeElement(
    item :VehicleType
): JSX.Element {
    return <Grid id = {item.id}>
        <strong>Vehicle Type: { item.name }</strong>
        <HourPriceElement { ...item.pricePerHour } />
        <KilometerPriceElement { ...item.pricePerKilometer } />
    </Grid>
}

export default VehicleTypeElement