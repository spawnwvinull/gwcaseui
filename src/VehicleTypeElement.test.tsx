import React from 'react';
import { render } from '@testing-library/react';
import { VehicleType, Price } from "./interfaces/Data"
import VehicleTypeElement from './VehicleTypeElement';

test('renders an subscription overview of vehicle types', () => {
  // arrange
  const hourPrice: Price = {
    id: "",
    amount: 10,
    currency: "Euro"
  }

  const kilometerPrice: Price = { 
    id: "",
    amount: .2,
    currency: "Euro"

  }
  const testVehicleType: VehicleType = {
    id: "", 
    name: "Small",
    pricePerHour: hourPrice,
    pricePerKilometer: kilometerPrice
  };

  const { getByText } = render(<VehicleTypeElement { ...testVehicleType }/>);
  const textElement = getByText('Vehicle Type: Small');
  expect(textElement).toBeInTheDocument();
});
