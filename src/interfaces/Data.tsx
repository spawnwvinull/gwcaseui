  export interface Subscriptions {
    subscriptionItems: Subscription[]
  }
  
  export interface Subscription {
    id: string,
    name: string;
    vehicleTypes: VehicleType[]
  }

  export interface VehicleType {
      id: string,
      name: string,
      pricePerHour: Price,
      pricePerKilometer: Price
  }

  export interface Price {
      id: string, 
      amount: number,
      currency: string,
  }

  export interface ActualSubscription {
    SubscriptionName: string,
    VehicleTypeName: string,
    NumberOfHours: number,
    NumberOfKilometers: number
  }

  export interface DropdownElements {
    SubscriptionNames: string[],
    VehicleTypeNames: string[]
  }