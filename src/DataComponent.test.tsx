import { getSubscriptionsPromise } from "./DataComponent";
import { sendDataForCalculation } from "./DataComponent";
import { Subscriptions } from "./interfaces/Data";
jest.mock("./DataComponent");

describe("Get subscriptions from the web api backend", () => {
  test("gets 3 subscriptions from api", done => {
    // arrange
    const subscriptionUrl = "http://localhost:5000/subscription";

    // act
    const promise = getSubscriptionsPromise(subscriptionUrl);

    promise
      .then((data: Subscriptions) => {
        // assert
        expect(data.subscriptionItems.length).toBe(3);
        done();
      })
      .catch(error => {
        throw Error("Should not get into reject state" + error);
      });
  });

  test("throw error when something goes wrong", done => {
    // arrange
    const imageUrl = "";

    // act
    const promise = getSubscriptionsPromise(imageUrl);

    promise
      .then(() => {
        throw Error("Should not get in resolved state");
      })
      .catch(error => {
        // assert
        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        expect(error).toBeDefined;
      });
    done();
  });
});

describe("send data to api for price calculation", () => {
  test("post data to the correct url", done => {
     // arrange
     const priceCalculationUrl = "http://localhost:5000/subscription";
     const data = {
      SubscriptionName: "Occasional",
      VehicleTypeName: "Small",
      NumberOfHours: 24,
      NumberOfKilometers: 100
     }

     // act
     const promise = sendDataForCalculation(priceCalculationUrl, data);
 
     promise
       .then((data: number) => {
         // assert
         expect(data).toBeGreaterThan(0);
         expect(data).toBe(608.16)
         done();
       })
       .catch(error => {
         throw Error("Should not get into reject state" + error);
       });
  });
  test("throw error when something goes wrong", done => {
    // arrange
    const subscriptionUrl = "";
    const data = {
      SubscriptionName: "Occasional",
      VehicleTypeName: "Small",
      NumberOfHours: 24,
      NumberOfKilometers: 100
     }
    // act
    const promise = sendDataForCalculation(subscriptionUrl, data);
    promise
      .then(() => {
        throw Error("Should not get in resolved state");
      })
      .catch(error => {
        // assert
        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        expect(error).toBeDefined;
      });
    done();
    });
});