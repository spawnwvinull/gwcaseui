import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders an subscription overview of vehicle types', () => {
  const { getByText } = render(<App />);
  const textElement = getByText('GreenWheels vehicle subcription overview');
  expect(textElement).toBeInTheDocument();
});
