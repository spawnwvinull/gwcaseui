import axios from "axios";
import { Subscriptions, ActualSubscription } from "./interfaces/Data";

export function getSubscriptionsPromise(subscriptionUrl: string): Promise<Subscriptions> {
  return new Promise<Subscriptions>((resolve, reject) => {
    axios
      .get<Subscriptions>(subscriptionUrl)
      .then((response: { data: Subscriptions }) => resolve(response.data))
      .catch((error: any) => {
        reject(error);
      });
  });
}

export function sendDataForCalculation(priceCalculationUrl: string, data: ActualSubscription): Promise<number> {
  return new Promise<number>((resolve, reject) => {
  
    axios.defaults.adapter = require('axios/lib/adapters/http')
    axios
      .post(priceCalculationUrl, data)
      .then((response: {data: number }) =>resolve(response.data))
      .catch((error: any) => {
        reject(error)
      })
  })
}