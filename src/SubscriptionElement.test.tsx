import React from 'react';
import { render } from '@testing-library/react';
import SubScriptionElement from './SubscriptionElement';
import { Subscription } from "./interfaces/Data"

test('renders an subscription overview of vehicle types', () => {
  // arrange
  const testSubscription: Subscription = {
    id: "", 
    name: "Occasional",
    vehicleTypes: []
  };

  const { getByText } = render(<SubScriptionElement { ...testSubscription }/>);
  const textElement = getByText('Occasional');
  expect(textElement).toBeInTheDocument();
});
