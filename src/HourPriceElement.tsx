import { Grid } from "@material-ui/core";
import { Price } from "./interfaces/Data";
import React from "react";
import { resolveCurrency, resolveAmount } from "./Utils"


function HourPriceElement(item: Price) :JSX.Element {
    return (
        <Grid id={item.id}>
            Per Hour {resolveCurrency(item.currency) } { resolveAmount(item.amount) }
        </Grid>
    )
}

export default HourPriceElement 