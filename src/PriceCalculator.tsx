import React, { useState } from "react";
import Grid from "@material-ui/core/Grid";
import { ActualSubscription, DropdownElements} from './interfaces/Data';
import config from "react-global-configuration";
import { sendDataForCalculation } from "./DataComponent"
import { resolveAmount } from "./Utils"


function PriceCalculator(item: DropdownElements): JSX.Element {
    const [price, setPrice] = useState<string>();

    function handleSubmit(event: any) {
        event.preventDefault();
        const data: any = new FormData(event.target);
    
        let actualSubscription: ActualSubscription = {
            SubscriptionName: data.get('SubscriptionName'),
            VehicleTypeName: data.get('VehicleTypeName'),
            NumberOfKilometers: parseInt(data.get('NumberOfKilometers')),
            NumberOfHours: parseInt(data.get('NumberOfHours'))
        }
    
        const dataUrl = config.get("priceCalculationUrl");
        sendDataForCalculation(dataUrl, actualSubscription)
            .then((data: number) => {
                console.log(data)
                setPrice("Price is: " + resolveAmount(data))
            })
            .catch(error => {
                console.log(error);
                setPrice("Input not valid")
            })
    }
    
    return <Grid>
        <h1>Price calculator</h1>
        <form onSubmit={handleSubmit}>
            <Grid>
                SubscriptionName: <select name="SubscriptionName"> {item.SubscriptionNames.map((x: string, y: any) => <option key={y} >{x}</option>)}</select>
            </Grid>
           <Grid>
                VehicleType: <select name="VehicleTypeName"> {item.VehicleTypeNames.map((x: string, y: any) => <option key={y} >{x}</option>)}</select>
           </Grid>
           <Grid>
                Number of kilometers: <input id="NumberOfKilometers" name="NumberOfKilometers"></input>
           </Grid>
           <Grid>
                Number of hours: <input id="NumberOfHours" name="NumberOfHours"></input>
           </Grid>
           <button>Submit</button>
        </form>

        <Grid>
            {price}
        </Grid>
    </Grid>
}

export default PriceCalculator